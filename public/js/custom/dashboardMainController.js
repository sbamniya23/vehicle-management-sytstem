var ongoingTrips ={};
    
customApp.controller('dashboardMainController', function ($scope,$location, $http) {
	$http.get("/authentication/admin")
      .success(function(response,status,headers,config){
          if (response.error) {
          	$location.path("/");
          }else{
              
          }
    });
	$http.post("/get_ongoing_trips")
        .success(function(response,status,headers,config){
            ongoingTrips = response;

        });

    $scope.logout = function() {

		$http.post("/logout")
		.success(function(response,status,headers,config){
          console.log(response);
		    	$location.path("/");
		}); 
	} 
});